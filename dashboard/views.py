# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from members.models import Member
from members.forms import AddFriendForm
from members.forms import AcceptFriendForm



@login_required
def index(request):
    current_member = request.user.member
    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()

    if request.method == "POST":
        add_friend_form = AddFriendForm(member=current_member, data=request.POST)
        if add_friend_form.is_valid():

            for member in add_friend_form.cleaned_data["members"]:
                member.friend_requests.add(current_member)
            add_friend_form = AddFriendForm(member=current_member)
            accept_friend_form = AcceptFriendForm(member=current_member)

            messages.success(request, "Demandes d'ajout effectuées avec succès")

    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        accept_friend_form = AcceptFriendForm(member=current_member)

    context = {'friends': friends,
               'friend_requests': friend_requests,
               'add_friend_form': add_friend_form,
               'accept_friend_form' : accept_friend_form}
    return render(request, "dashboard/index.html", context)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")
