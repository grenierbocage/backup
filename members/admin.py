# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.contrib import admin
from .models import Member
# Register your models here.


class MemberAdmin(admin.ModelAdmin):
    search_fields = ['user__username']


admin.site.register(Member, MemberAdmin)
